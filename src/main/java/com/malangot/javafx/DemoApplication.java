package com.malangot.javafx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

@SpringBootApplication
public class DemoApplication extends Application {
    private ConfigurableApplicationContext springContext;
    private Parent rootNode;
    private FXMLLoader fxmlLoader;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() throws Exception {
        this.springContext = SpringApplication.run(DemoApplication.class);
        this.fxmlLoader = new FXMLLoader();
        this.fxmlLoader.setControllerFactory(this.springContext::getBean);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.fxmlLoader.setLocation(this.getClass().getResource("/fxml/sample.fxml"));
        this.rootNode = this.fxmlLoader.load();

        primaryStage.setTitle("Hello World");
        Scene scene = new Scene(this.rootNode, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() {
        this.springContext.stop();
    }

}
